use std::cmp::Ordering;
use flo_curves::{
    bezier::{de_casteljau3, de_casteljau4, Curve},
    BezierCurve, Coord2, Coordinate2D,
};
use geo::{
    algorithm::bool_ops::BooleanOps, Coord, CoordsIter, Geometry, GeometryCollection, Line,
    LineString, MultiLineString, MultiPolygon, Polygon, Rect,
};
use geo_normalized::Normalized;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::convert::From;
use svgtypes::{PathParser, PathSegment, PointsParser};
use thiserror::Error;
use xml::attribute::OwnedAttribute;
use xml::name::OwnedName;
use xml::reader::{EventReader, XmlEvent};

/// Number of segments per unit of a curve's length
const SEGS_PER_UNIT: f64 = 1.0;
const ELLIPSE_DETAIL_SCALE: f64 = 16.3636;

#[derive(Error, Debug)]
pub enum SvgError {
    #[error("Could not read SVG file: {0}")]
    FileError(#[from] std::io::Error),
    #[error("failed to parse: {0}")]
    ParseError(#[from] std::num::ParseFloatError),
    #[error("The SVG could not be parsed to a valid Geometry type: {0}")]
    SvgInvalidType(String),
    #[error("The SVG could only be parsed as a GEOMETRYCOLLECTION")]
    SvgGeomCollectionForGeometry,
    #[error("The SVG input is invalid: {0}")]
    InvalidSvgError(&'static str),
}

#[derive(PartialEq, Clone, Debug, Serialize, Deserialize)]
pub struct SvgFile {
    pub elements: Vec<StyledGeometryCollection>,
}

#[derive(PartialEq, Clone, Debug, Serialize, Deserialize)]
pub struct StyledGeometryCollection {
    pub geometry: GeometryCollection<f64>,
    pub style: HashMap<String, String>,
}

impl StyledGeometryCollection {
    pub fn new(geometry: GeometryCollection<f64>, style: HashMap<String, String>) -> Self {
        Self { geometry, style }
    }
}

#[derive(PartialEq, Clone, Debug, Serialize, Deserialize)]
pub struct StyledGeometry {
    pub geometry: Geometry<f64>,
    pub style: HashMap<String, String>,
}

impl StyledGeometry {
    pub fn new(geometry: Geometry<f64>, style: HashMap<String, String>) -> Self {
        Self { geometry, style }
    }
}

impl TryFrom<StyledGeometryCollection> for StyledGeometry {
    type Error = SvgError;

    fn try_from(mut gc: StyledGeometryCollection) -> Result<Self, Self::Error> {
        if gc.geometry.0.len() == 1 {
            Ok(StyledGeometry::new(gc.geometry.0.remove(0), gc.style))
        } else {
            Err(SvgError::SvgGeomCollectionForGeometry)
        }
    }
}

/// Returns a GeometryCollection parsed from the submitted SVG element
///
/// **Note** this function does not parse a full SVG string (e.g., `<svg xmlns="http://www.w3.org/2000/svg"><path d="M0 0L10 0L10 10L0 10Z"/></svg>`), it only parses the individual shape elements (e.g., `<path d="M0 0L10 0L10 10L0 10Z"/>`).  The following SVG elements are supported and produce the specified Geometry types:
///
/// * \<path\> &rarr; GeometryCollection
/// * \<polygon\> &rarr; GeometryCollection with a single Polygon
/// * \<polyline\> &rarr; GeometryCollection with a single LineString
/// * \<rect\> &rarr; GeometryCollection with a single Polygon
/// * \<line\> &rarr; GeometryCollection with a single Line
///
///
/// # Examples
///
/// Parsing a `<path>` element:
///
/// ```rust
/// use geo::{ Polygon, polygon };
/// use geo_svg_io::geo_svg_reader::svg_to_geometry_collection;
///
/// let poly: Polygon<f64> = polygon!(
///         exterior: [
///             (x: 60.0, y: 0.0),
///             (x: 0.0, y: 0.0),
///             (x: 0.0, y: 60.0),
///             (x: 60.0, y: 60.0),
///             (x: 60.0, y: 0.0),],
///         interiors:[[
///             (x: 10.50, y: 40.0),
///             (x: 10.0, y: 10.0),
///             (x: 40.0, y: 1.0),
///             (x: 40.0, y: 40.0),
///             (x: 10.50, y: 40.0),]
///             ]
///         )
///         .into();
/// let svg_string =
///             String::from(r#"<path d="M0 0L0 60L60 60L60 0L0 0M10 10L40 1L40 40L10.5 40L10 10"/>"#);
///
/// let parsed_svg = svg_to_geometry_collection(&svg_string, false);
/// assert_eq!(parsed_svg.is_ok(), true);
///
/// // Unwrap the GeometryCollection result
/// let geom = parsed_svg.ok().unwrap();
/// assert_eq!(1, geom.geometry.0.len());
///
/// // Read the geometry as a Polygon
/// let pl = Polygon::try_from(geom.geometry.0[0].clone());
/// assert_eq!(true, pl.is_ok());
/// assert_eq!(poly, pl.unwrap());
/// ```
///
/// Parsing a `<polygon>` element:
///
/// ```rust
/// use geo::{ Polygon, polygon };
/// use geo_svg_io::geo_svg_reader::svg_to_geometry_collection;
///
/// let poly: Polygon<f64> = polygon!(
///         exterior: [
///             (x: 0.0, y: 0.0),
///             (x: 0.0, y: 60.0),
///             (x: 60.0, y: 60.0),
///             (x: 60.0, y: 0.0),
///             (x: 0.0, y: 0.0),],
///         interiors:[]
///         )
///         .into();
///
/// let svg_string = String::from(r#"<polygon points="0, 0 60, 0 60, 60 0, 60 0, 0"/>"#);
///
/// let parsed_svg = svg_to_geometry_collection(&svg_string, false);
/// assert_eq!(parsed_svg.is_ok(), true);
///
/// // Unwrap the GeometryCollection result
/// let geom = parsed_svg.ok().unwrap();
/// assert_eq!(1, geom.geometry.0.len());
///
/// // Read the geometry as a Polygon
/// let pl = Polygon::try_from(geom.geometry.0[0].clone());
/// assert_eq!(true, pl.is_ok());
/// assert_eq!(poly, pl.unwrap());
/// ```
///
pub fn svg_to_geometry_collection(
    svg: &str,
    parse_style: bool,
) -> Result<StyledGeometryCollection, SvgError> {
    let parser = EventReader::new(svg.as_bytes());
    for e in parser.into_iter().flatten() {
        if let XmlEvent::StartElement {
            name, attributes, ..
        } = e
        {
            let mut style = HashMap::new();
            if parse_style {
                if let Some(found) = attributes.iter().find(|a| a.name.local_name == "style") {
                    style = found
                        .value
                        .split(';')
                        .filter_map(|v| {
                            v.split_once(':')
                                .map(|(s1, s2)| (s1.to_owned(), s2.to_owned()))
                        })
                        .collect();
                }
                if !style.contains_key("fill-opacity") {
                    if let Some(opac) = attributes
                        .iter()
                        .find(|a| a.name.local_name == "fill-opacity")
                    {
                        style.insert("fill-opacity".to_string(), opac.value.clone());
                    }
                }
            }

            return Ok(StyledGeometryCollection::new(
                svg_element_to_geometry_collection(name, attributes)?,
                style,
            ));
        }
    }

    Err(SvgError::SvgInvalidType(format!(
        "SVG line failed: {:?}",
        svg
    )))
}

fn svg_element_to_geometry_collection(
    name: OwnedName,
    attributes: Vec<OwnedAttribute>,
) -> Result<GeometryCollection<f64>, SvgError> {
    if attributes.iter().any(|a| a.name.local_name == "transform") {
        return Err(SvgError::InvalidSvgError(
            "Unsupported attribute: `transform`. Please apply all your transforms!",
        ));
    }
    // An SVG path element
    if name.local_name == "path" {
        for attr in attributes.iter() {
            if attr.name.local_name == "d" {
                let res = svg_d_path_to_geometry_collection(&attr.value)?;
                return Ok(res);
            }
        }
    }
    // An SVG polygon
    else if name.local_name == "polygon" {
        for attr in attributes.iter() {
            if attr.name.local_name == "points" {
                let res = svg_polygon_to_geometry(&attr.value)?;
                return Ok(res.into());
            }
        }
    }
    // An SVG polyline
    else if name.local_name == "polyline" {
        for attr in attributes.iter() {
            if attr.name.local_name == "points" {
                let res = svg_polyline_to_geometry(&attr.value)?;
                return Ok(res.into());
            }
        }
    }
    // An SVG rect
    else if name.local_name == "rect" {
        let mut x: Option<f64> = None;
        let mut y: Option<f64> = None;
        let mut width: Option<f64> = None;
        let mut height: Option<f64> = None;

        for attr in attributes.iter() {
            if attr.name.local_name == "x" {
                let x_val = attr.value.parse::<f64>()?;
                x = Some(x_val);
            } else if attr.name.local_name == "y" {
                let y_val = attr.value.parse::<f64>()?;
                y = Some(y_val);
            } else if attr.name.local_name == "width" {
                let width_val = attr.value.parse::<f64>()?;
                width = Some(width_val);
            } else if attr.name.local_name == "height" {
                let height_val = attr.value.parse::<f64>()?;
                height = Some(height_val);
            }
        }

        let rect = svg_rect_to_geometry(
            x.ok_or(SvgError::InvalidSvgError("No x for rect"))?,
            y.ok_or(SvgError::InvalidSvgError("No y for rect"))?,
            width.ok_or(SvgError::InvalidSvgError("No width for rect"))?,
            height.ok_or(SvgError::InvalidSvgError("No height for rect"))?,
        )?;

        return Ok(rect.into());
    }
    // An SVG ellipse
    else if name.local_name == "ellipse" {
        let mut x: Option<f64> = None;
        let mut y: Option<f64> = None;
        let mut width: Option<f64> = None;
        let mut height: Option<f64> = None;

        for attr in attributes.iter() {
            if attr.name.local_name == "cx" {
                let x_val = attr.value.parse::<f64>()?;
                x = Some(x_val);
            } else if attr.name.local_name == "cy" {
                let y_val = attr.value.parse::<f64>()?;
                y = Some(y_val);
            } else if attr.name.local_name == "rx" {
                let width_val = attr.value.parse::<f64>()?;
                width = Some(width_val);
            } else if attr.name.local_name == "ry" {
                let height_val = attr.value.parse::<f64>()?;
                height = Some(height_val);
            }
        }

        let ellipse = svg_ellipse_to_geometry(
            x.ok_or(SvgError::InvalidSvgError("No cx for ellipse"))?,
            y.ok_or(SvgError::InvalidSvgError("No cy for ellipse"))?,
            width.ok_or(SvgError::InvalidSvgError("No rx for ellipse"))?,
            height.ok_or(SvgError::InvalidSvgError("No ry for ellipse"))?,
        )?;

        return Ok(ellipse.into());
    }
    // An SVG circle
    else if name.local_name == "circle" {
        // Almost an ellipse but with a single xy radius
        let mut x: Option<f64> = None;
        let mut y: Option<f64> = None;
        let mut rad: Option<f64> = None;

        for attr in attributes.iter() {
            if attr.name.local_name == "cx" {
                let x_val = attr.value.parse::<f64>()?;
                x = Some(x_val);
            } else if attr.name.local_name == "cy" {
                let y_val = attr.value.parse::<f64>()?;
                y = Some(y_val);
            } else if attr.name.local_name == "r" {
                let rad_val = attr.value.parse::<f64>()?;
                rad = Some(rad_val);
            }
        }

        let r = rad.ok_or(SvgError::InvalidSvgError("No r for ellipse"))?;
        let ellipse = svg_ellipse_to_geometry(
            x.ok_or(SvgError::InvalidSvgError("No cx for ellipse"))?,
            y.ok_or(SvgError::InvalidSvgError("No cy for ellipse"))?,
            r,
            r,
        )?;

        return Ok(ellipse.into());
    }
    // An SVG line
    else if name.local_name == "line" {
        let mut start_x: Option<f64> = None;
        let mut start_y: Option<f64> = None;
        let mut end_x: Option<f64> = None;
        let mut end_y: Option<f64> = None;

        for attr in attributes.iter() {
            if attr.name.local_name == "x1" {
                let start_x_val = attr.value.parse::<f64>()?;
                start_x = Some(start_x_val);
            } else if attr.name.local_name == "y1" {
                let start_y_val = attr.value.parse::<f64>()?;
                start_y = Some(start_y_val);
            } else if attr.name.local_name == "x2" {
                let end_x_val = attr.value.parse::<f64>()?;
                end_x = Some(end_x_val);
            } else if attr.name.local_name == "y2" {
                let end_y_val = attr.value.parse::<f64>()?;
                end_y = Some(end_y_val);
            }
        }

        return Ok(svg_line_to_geometry(
            &start_x.ok_or(SvgError::InvalidSvgError("No x1 for line"))?,
            &start_y.ok_or(SvgError::InvalidSvgError("No y1 for line"))?,
            &end_x.ok_or(SvgError::InvalidSvgError("No x2 for line"))?,
            &end_y.ok_or(SvgError::InvalidSvgError("No y2 for line"))?,
        )
        .into());
    }
    Err(SvgError::SvgInvalidType(format!(
        "{:?} {:?}",
        name.local_name, attributes
    )))
}

/// Parses an entire SVG file and returns all elements as GeometryCollection, returning
/// an error on first error.
pub fn svg_file_to_geometry_collections(
    data: &str,
    parse_style: bool,
) -> Result<SvgFile, SvgError> {
    let mut result = vec![];

    let parser = EventReader::new(data.as_bytes());
    let mut ignoring = String::with_capacity(16);
    for e in parser.into_iter().flatten() {
        match e {
            XmlEvent::StartElement {
                name, attributes, ..
            } => {
                // skip but don't ignore inners
                if !ignoring.is_empty()
                    || vec!["svg", "g", "?xml", "namedview", "!DOCTYPE"]
                        .contains(&name.local_name.as_str())
                {
                    continue;
                }
                // skip and ignore inners
                if vec!["script", "metadata", "defs", "desc"].contains(&name.local_name.as_str()) {
                    ignoring = name.local_name;
                    continue;
                }

                let mut style = HashMap::new();
                if parse_style {
                    if let Some(found) = attributes.iter().find(|a| a.name.local_name == "style") {
                        style = found
                            .value
                            .split(';')
                            .filter_map(|v| {
                                v.split_once(':')
                                    .map(|(s1, s2)| (s1.to_owned(), s2.to_owned()))
                            })
                            .collect();
                    }
                    if !style.contains_key("fill-opacity") {
                        if let Some(opac) = attributes
                            .iter()
                            .find(|a| a.name.local_name == "fill-opacity")
                        {
                            style.insert("fill-opacity".to_string(), opac.value.clone());
                        }
                    }
                }

                result.push(StyledGeometryCollection::new(
                    svg_element_to_geometry_collection(name, attributes)?,
                    style,
                ));
            }
            XmlEvent::EndElement { name, .. } => {
                if name.local_name == ignoring {
                    ignoring.clear();
                }
            }
            _ => {}
        }
    }

    Ok(SvgFile { elements: result })
}

/// Returns a Geometry parsed from the submitted SVG element
///
/// **Note** this function does not parse a full SVG string (e.g., `<svg xmlns="http://www.w3.org/2000/svg"><path d="M0 0L10 0L10 10L0 10Z"/></svg>`), it only parses the individual shape elements (e.g., `<path d="M0 0L10 0L10 10L0 10Z"/>`).  The following SVG elements are supported and produce the specified Geometry types:
///
/// * \<path\> &rarr; Geometry with the autodetected Geometry type
/// * \<polygon\> &rarr; Polygon
/// * \<polyline\> &rarr; LineString
/// * \<rect\> &rarr; Polygon
/// * \<line\> &rarr; Line
///
///
/// # Examples
///
/// Parsing a `<path>` element:
///
/// ```rust
/// use geo::{ Polygon, polygon };
/// use geo_svg_io::geo_svg_reader::svg_to_geometry;
///
/// let poly: Polygon<f64> = polygon!(
///     exterior: [
///         (x: 60.0, y: 0.0),
///         (x: 0.0, y: 0.0),
///         (x: 0.0, y: 60.0),
///         (x: 60.0, y: 60.0),],
///     interiors:[[
///         (x: 10.50, y: 40.0),
///         (x: 10.0, y: 10.0),
///         (x: 40.0, y: 1.0),
///         (x: 40.0, y: 40.0),
///         (x: 10.50, y: 40.0),]
///         ]
///     );
/// let svg_string =
///     String::from(r#"<path d="M0 0L0 60L60 60L60 0L0 0M10 10L40 1L40 40L10.5 40L10 10"/>"#);
///
/// let parsed_svg = svg_to_geometry(&svg_string, false);
/// assert!(parsed_svg.is_ok());
/// let parsed_poly = Polygon::try_from(parsed_svg.ok().unwrap().geometry);
/// assert!(parsed_poly.is_ok());
/// assert_eq!(poly, parsed_poly.unwrap());
/// ```
///
/// Parsing a `<polygon>` element:
///
/// ```rust
/// use geo::{ Polygon, polygon };
/// use geo_svg_io::geo_svg_reader::svg_to_geometry;
///
/// let poly: Polygon<f64> = polygon!(
///     exterior: [
///         (x: 0.0, y: 0.0),
///         (x: 0.0, y: 60.0),
///         (x: 60.0, y: 60.0),
///         (x: 60.0, y: 0.0),
///         (x: 0.0, y: 0.0),],
///     interiors:[]
///     );
/// let svg_string = String::from(r#"<polygon points="0, 0 60, 0 60, 60 0, 60 0, 0"/>"#);
///
/// let parsed_svg = svg_to_geometry(&svg_string, false);
/// assert!(parsed_svg.is_ok());
/// let parsed_poly = Polygon::try_from(parsed_svg.ok().unwrap().geometry);
/// assert!(parsed_poly.is_ok());
/// assert_eq!(poly, parsed_poly.unwrap());
/// ```
///
pub fn svg_to_geometry(svg: &str, parse_style: bool) -> Result<StyledGeometry, SvgError> {
    svg_to_geometry_collection(svg, parse_style)?.try_into()
}

fn svg_polygon_to_geometry(point_string: &str) -> Result<Polygon<f64>, SvgError> {
    let points = PointsParser::from(point_string);
    let polygon = Polygon::new(
        LineString(
            points
                .map(|(x, y)| Coord { x, y })
                .collect::<Vec<Coord<f64>>>(),
        ),
        vec![],
    );

    if polygon.exterior().coords_count() == 0 {
        return Err(SvgError::InvalidSvgError("No exterior coords for polygon"));
    }
    Ok(polygon.normalized())
}

fn svg_polyline_to_geometry(point_string: &str) -> Result<LineString<f64>, SvgError> {
    let points = PointsParser::from(point_string);
    let linestring = LineString(
        points
            .map(|(x, y)| Coord { x, y })
            .collect::<Vec<Coord<f64>>>(),
    );

    if linestring.coords_count() == 0 {
        return Err(SvgError::InvalidSvgError("No coords for linestring"));
    }
    Ok(linestring)
}

fn svg_rect_to_geometry(x: f64, y: f64, width: f64, height: f64) -> Result<Rect<f64>, SvgError> {
    let max_x = x + width;
    let max_y = y + height;
    if x > max_x {
        return Err(SvgError::InvalidSvgError("x > max_x for rect"));
    }
    if y > max_y {
        return Err(SvgError::InvalidSvgError("y > max_y for rect"));
    }

    Ok(Rect::new(
        Coord::<f64> { x, y },
        Coord::<f64> { x: max_x, y: max_y },
    ))
}

struct DegCoord {
    deg: f64,
    coord: (f64, f64),
}

impl Into<Coord<f64>> for DegCoord {
    fn into(self) -> Coord<f64> {
        self.coord.into()
    }
}

fn svg_ellipse_to_geometry(
    x: f64,
    y: f64,
    width: f64,
    height: f64,
) -> Result<Polygon<f64>, SvgError> {
    let avg_size = (width + height) * 0.5;
    let details = (avg_size * ELLIPSE_DETAIL_SCALE).max(3.0);
    let details_to_deg = 360.0 / details;
    let details = details as usize;
    let mut circle = Vec::with_capacity(details);

    for i in 0..details {
        let deg = (i as f64) * details_to_deg;
        let px = x + (width * deg.cos());
        let py = y + (height * deg.sin());
        circle.push(DegCoord { deg: (py - y).atan2(px - x), coord: (px, py) });
    }

    // Because the above loop adds the verts in a strange order,
    // we need to unwind them.
    circle.sort_unstable_by(|d1, d2| d1.deg.partial_cmp(&d2.deg).unwrap_or(Ordering::Equal));
    Ok(Polygon::new(LineString::from(circle), vec![]).normalized())
}

fn svg_line_to_geometry(start_x: &f64, start_y: &f64, end_x: &f64, end_y: &f64) -> Line<f64> {
    Line::new(
        Coord::<f64> {
            x: *start_x,
            y: *start_y,
        },
        Coord::<f64> {
            x: *end_x,
            y: *end_y,
        },
    )
}

/// Parses the `d`-string from an SVG `<path>` element into a GeometryCollection
///
///
/// # Examples
///
/// ```rust
/// use geo_svg_io::geo_svg_reader::svg_d_path_to_geometry_collection;
/// use geo::{polygon, Polygon};
///
/// let poly = polygon!(
///         exterior: [
///             (x: 60.0, y: 0.0),
///             (x: 0.0, y: 0.0),
///             (x: 0.0, y: 60.0),
///             (x: 60.0, y: 60.0),
///             (x: 60.0, y: 0.0),],
///         interiors:[[
///             (x: 10.50, y: 40.0),
///             (x: 10.0, y: 10.0),
///             (x: 40.0, y: 1.0),
///             (x: 40.0, y: 40.0),
///             (x: 10.50, y: 40.0),]
///             ]
///         );
///
/// let svg_string = String::from("M0 0l0 60l60 0L60 0L0 0M10 10L40 1L40 40L10.5 40L10 10");
/// let parsed_svg = svg_d_path_to_geometry_collection(&svg_string);
/// assert_eq!(parsed_svg.is_ok(), true);
///
/// // Unwrap the GeometryCollection result
/// let geom = parsed_svg.ok().unwrap();
/// assert_eq!(1, geom.0.len());
///
/// // Read the geometry as a Polygon
/// let pl = Polygon::try_from(geom.0[0].clone());
/// assert_eq!(true, pl.is_ok());
/// assert_eq!(pl.unwrap(), poly);
/// ```
///
pub fn svg_d_path_to_geometry_collection(svg: &str) -> Result<GeometryCollection<f64>, SvgError> {
    // We will collect the separate paths (from M to M) into segments for parsing
    let mut path_segments = vec![] as Vec<Vec<Coord<f64>>>;
    let mut segment_count = 0;
    let mut first_segment = true;
    let zero_coord = Coord { x: 0.0, y: 0.0 }; // Default values to be added to relative coords
    let mut last_point: Option<Coord<f64>> = None; // Store last point for relative coordinates
    let mut last_control_point: Option<Coord2> = None; // Store last control point for S and T coordinates
    let p = PathParser::from(svg);
    for token in p {
        let t = token.unwrap();
        match t {
            PathSegment::MoveTo { .. } => {
                path_segments.push(vec![] as Vec<Coord<f64>>);
                if !first_segment {
                    segment_count += 1;
                } else {
                    first_segment = false;
                }
                let coord = Coord {
                    x: if t.is_relative() {
                        t.x().unwrap() + last_point.unwrap_or(zero_coord).x
                    } else {
                        t.x().unwrap()
                    },
                    y: if t.is_relative() {
                        t.y().unwrap() + last_point.unwrap_or(zero_coord).y
                    } else {
                        t.y().unwrap()
                    },
                };
                last_point = Some(coord);
                path_segments[segment_count].push(coord);
            }
            PathSegment::LineTo { .. } => {
                let coord = Coord {
                    x: if t.is_relative() {
                        t.x().unwrap() + last_point.unwrap_or(zero_coord).x
                    } else {
                        t.x().unwrap()
                    },
                    y: if t.is_relative() {
                        t.y().unwrap() + last_point.unwrap_or(zero_coord).y
                    } else {
                        t.y().unwrap()
                    },
                };
                last_point = Some(coord);
                path_segments[segment_count].push(coord);
            }
            PathSegment::HorizontalLineTo { .. } => {
                let coord = Coord {
                    x: if t.is_relative() {
                        t.x().unwrap() + last_point.unwrap_or(zero_coord).x
                    } else {
                        t.x().unwrap()
                    },
                    y: last_point.unwrap_or(zero_coord).y,
                };
                last_point = Some(coord);
                path_segments[segment_count].push(coord);
            }
            PathSegment::VerticalLineTo { .. } => {
                let coord = Coord {
                    x: last_point.unwrap_or(zero_coord).x,
                    y: if t.is_relative() {
                        t.y().unwrap() + last_point.unwrap_or(zero_coord).y
                    } else {
                        t.y().unwrap()
                    },
                };
                last_point = Some(coord);
                path_segments[segment_count].push(coord);
            }
            PathSegment::CurveTo {
                x,
                x1,
                x2,
                y,
                y1,
                y2,
                abs,
            } => {
                let last = last_point.unwrap_or(zero_coord);
                let start_point = calculate_svg_coord2(last.x, last.y, last, true);
                let control_1 = calculate_svg_coord2(x1, y1, last, abs);
                let control_2 = calculate_svg_coord2(x2, y2, last, abs);
                last_control_point = Some(control_2);
                let end_point = calculate_svg_coord2(x, y, last, abs);
                let end = Coord {
                    x: end_point.x(),
                    y: end_point.y(),
                };
                last_point = Some(end);

                let curve_segs = Curve {
                    start_point,
                    end_point,
                    control_points: (control_1, control_2),
                }
                .estimate_length()
                    * SEGS_PER_UNIT;

                for x in 1..curve_segs as i32 {
                    let arc_point = de_casteljau4(
                        x as f64 / curve_segs,
                        start_point,
                        control_1,
                        control_2,
                        end_point,
                    );
                    path_segments[segment_count].push(Coord {
                        x: arc_point.x(),
                        y: arc_point.y(),
                    });
                }
                path_segments[segment_count].push(end);
            }
            PathSegment::SmoothCurveTo { x2, x, y2, y, abs } => {
                let last = last_point.unwrap_or(zero_coord);
                let start_point = calculate_svg_coord2(last.x, last.y, last, true);
                let control_1 = reflect_point(last, last_control_point.unwrap_or(Coord2(0., 0.)));
                let control_2 = calculate_svg_coord2(x2, y2, last, abs);
                last_control_point = Some(control_2);
                let end_point = calculate_svg_coord2(x, y, last, abs);
                let end = Coord {
                    x: end_point.x(),
                    y: end_point.y(),
                };
                last_point = Some(end);

                let curve_segs = Curve {
                    start_point,
                    end_point,
                    control_points: (control_1, control_2),
                }
                .estimate_length()
                    * SEGS_PER_UNIT;

                for x in 1..curve_segs as i32 {
                    let arc_point = de_casteljau4(
                        x as f64 / curve_segs,
                        start_point,
                        control_1,
                        control_2,
                        end_point,
                    );
                    path_segments[segment_count].push(Coord {
                        x: arc_point.x(),
                        y: arc_point.y(),
                    });
                }
                path_segments[segment_count].push(end);
            }
            PathSegment::Quadratic { x1, x, y1, y, abs } => {
                let last = last_point.unwrap_or(zero_coord);
                let start_point = calculate_svg_coord2(last.x, last.y, last, true);
                let control_1 = calculate_svg_coord2(x1, y1, last, abs);
                last_control_point = Some(control_1);
                let end_point = calculate_svg_coord2(x, y, last, abs);
                let end = Coord {
                    x: end_point.x(),
                    y: end_point.y(),
                };
                last_point = Some(end);

                let curve_segs = Curve {
                    start_point,
                    end_point,
                    control_points: (control_1, control_1),
                }
                .estimate_length()
                    * SEGS_PER_UNIT;

                for x in 1..curve_segs as i32 {
                    let arc_point =
                        de_casteljau3(x as f64 / curve_segs, start_point, control_1, end_point);
                    path_segments[segment_count].push(Coord {
                        x: arc_point.x(),
                        y: arc_point.y(),
                    });
                }
                path_segments[segment_count].push(end);
            }
            PathSegment::SmoothQuadratic { x, y, abs } => {
                let last = last_point.unwrap_or(zero_coord);
                let start_point = calculate_svg_coord2(last.x, last.y, last, true);
                let control_1 = reflect_point(last, last_control_point.unwrap_or(Coord2(0., 0.)));
                last_control_point = Some(control_1);
                let end_point = calculate_svg_coord2(x, y, last, abs);
                let end = Coord {
                    x: end_point.x(),
                    y: end_point.y(),
                };
                last_point = Some(end);

                let curve_segs = Curve {
                    start_point,
                    end_point,
                    control_points: (control_1, control_1),
                }
                .estimate_length()
                    * SEGS_PER_UNIT;

                for x in 1..curve_segs as i32 {
                    let arc_point =
                        de_casteljau3(x as f64 / curve_segs, start_point, control_1, end_point);
                    path_segments[segment_count].push(Coord {
                        x: arc_point.x(),
                        y: arc_point.y(),
                    });
                }
                path_segments[segment_count].push(end);
            }
            // TODO: PathSegment::EllipticalArc
            PathSegment::ClosePath { .. } => {
                let coord = Coord {
                    x: path_segments[segment_count][0].x,
                    y: path_segments[segment_count][0].y,
                };
                last_point = Some(coord);
                path_segments[segment_count].push(coord);
            }
            _ => last_point = None,
        }
    }
    if path_segments.is_empty() {
        return Err(SvgError::InvalidSvgError("d path would be empty"));
    }
    Ok(parse_path_segments_to_geom(&path_segments))
}

/// Parses the `d`-string from an SVG `<path>` element into a single Geometry
///
///
/// # Examples
///
/// ```rust
/// use geo_svg_io::geo_svg_reader::svg_d_path_to_geometry;
/// use geo::{polygon, Polygon};
///
/// let poly = polygon!(
///         exterior: [
///             (x: 60.0, y: 0.0),
///             (x: 0.0, y: 0.0),
///             (x: 0.0, y: 60.0),
///             (x: 60.0, y: 60.0),
///             (x: 60.0, y: 0.0),],
///         interiors:[[
///             (x: 10.50, y: 40.0),
///             (x: 10.0, y: 10.0),
///             (x: 40.0, y: 1.0),
///             (x: 40.0, y: 40.0),
///             (x: 10.50, y: 40.0),]
///             ]
///         );
///
/// let svg_string = String::from("M0 0l0 60l60 0L60 0L0 0M10 10L40 1L40 40L10.5 40L10 10");
/// let parsed_svg = svg_d_path_to_geometry(&svg_string);
/// assert!(parsed_svg.is_ok());
/// let pl = Polygon::try_from(parsed_svg.ok().unwrap());
/// assert!(pl.is_ok());
/// assert_eq!(pl.unwrap(), poly);
/// ```
///
pub fn svg_d_path_to_geometry(svg: &str) -> Result<Geometry<f64>, SvgError> {
    let gc = svg_d_path_to_geometry_collection(svg)?;
    if gc.0.len() == 1 {
        return Ok(gc.0[0].clone());
    }
    Err(SvgError::SvgGeomCollectionForGeometry)
}

fn calculate_svg_coord2(x: f64, y: f64, last: Coord<f64>, abs: bool) -> Coord2 {
    Coord2(
        if abs { x } else { last.x + x },
        if abs { y } else { last.y + y },
    )
}

fn reflect_point(orig: Coord<f64>, pr: Coord2) -> Coord2 {
    let x_step = pr.x() - orig.x;
    let y_step = pr.y() - orig.y;

    Coord2(orig.x - x_step, orig.y - y_step)
}

fn parse_path_segments_to_geom(paths: &Vec<Vec<Coord<f64>>>) -> GeometryCollection<f64> {
    let mut lines = vec![] as Vec<Line<f64>>;
    let mut line_strings = vec![] as Vec<LineString<f64>>;
    let mut poly_line_strings = vec![] as Vec<LineString<f64>>;
    let mut polygons: MultiPolygon<f64> = (vec![] as Vec<Polygon<f64>>).into();

    for path in paths {
        let length = path.len();
        if length == 0 {
            continue;
        } else if length == 2 {
            lines.push(Line::new(path[0], path[1]));
        } else if !path.first().unwrap().eq(path.last().unwrap()) {
            line_strings.push(path.clone().into());
        } else {
            poly_line_strings.push(path.clone().into());
        }
    }

    if !poly_line_strings.is_empty() {
        if poly_line_strings.len() == 1 {
            polygons = Polygon::new(poly_line_strings[0].clone(), vec![]).into();
        } else {
            polygons = parse_polygon_rings_to_geom(&poly_line_strings);
        }
    }

    let number_of_geom_types = !lines.is_empty() as i32
        + !line_strings.is_empty() as i32
        + !poly_line_strings.is_empty() as i32;

    let mut geom_collection = vec![] as Vec<Geometry<f64>>;
    if !lines.is_empty() {
        let return_lines = map_lines_to_geometry(&lines);
        if number_of_geom_types == 1 {
            return GeometryCollection(vec![return_lines]);
        } else {
            geom_collection.push(return_lines);
        }
    } else if !line_strings.is_empty() {
        let return_line_strings = map_line_strings_to_geometry(&line_strings);
        if number_of_geom_types == 1 {
            return GeometryCollection(vec![return_line_strings]);
        } else {
            geom_collection.push(return_line_strings);
        }
    } else if !polygons.0.is_empty() {
        let return_polygons = map_polygons_to_geometry(polygons);
        if number_of_geom_types == 1 {
            return GeometryCollection(vec![return_polygons]);
        } else {
            geom_collection.push(return_polygons);
        }
    }

    GeometryCollection(geom_collection)
}

fn parse_polygon_rings_to_geom(rings: &Vec<LineString<f64>>) -> MultiPolygon<f64> {
    // Early return for empty vector
    if rings.is_empty() {
        return (vec![] as Vec<Polygon<f64>>).into();
    }

    let mut ring_iter = rings.iter();
    let mut result_poly = MultiPolygon(vec![Polygon::new(
        ring_iter.next().unwrap().clone(),
        vec![],
    )]);
    for ring in ring_iter {
        let poly = MultiPolygon(vec![Polygon::new(ring.clone(), vec![])]);
        result_poly = result_poly.xor(&poly);
    }
    result_poly.0.iter().map(|x| x.normalized()).collect()
}

fn map_lines_to_geometry(lines: &Vec<Line<f64>>) -> Geometry<f64> {
    if lines.len() == 1 {
        lines[0].into()
    } else {
        let multi_line: MultiLineString<f64> = lines
            .iter()
            .map(|x| LineString(vec![x.start, x.end]))
            .collect();
        multi_line.into()
    }
}

fn map_line_strings_to_geometry(line_strings: &Vec<LineString<f64>>) -> Geometry<f64> {
    if line_strings.len() == 1 {
        line_strings[0].clone().into()
    } else {
        let multi_line: MultiLineString<f64> = MultiLineString(line_strings.to_vec());
        multi_line.into()
    }
}

fn map_polygons_to_geometry(polys: MultiPolygon<f64>) -> Geometry<f64> {
    if polys.0.len() == 1 {
        polys.0[0].clone().into()
    } else {
        polys.into()
    }
}

/** Tests */

#[cfg(test)]
mod tests {
    use super::*;
    use crate::geo_svg_writer::ToSvg;
    use geo::{coord, line_string, polygon, Rect};

    #[test]
    fn can_convert_svg_path() {
        let poly = polygon!(
        exterior: [
            (x: 60.0, y: 0.0),
            (x: 0.0, y: 0.0),
            (x: 0.0, y: 60.0),
            (x: 60.0, y: 60.0),
            (x: 60.0, y: 0.0),],
        interiors:[[
            (x: 10.50, y: 40.0),
            (x: 10.0, y: 10.0),
            (x: 40.0, y: 1.0),
            (x: 40.0, y: 40.0),
            (x: 10.50, y: 40.0),]
            ]
        );
        let svg_string = String::from("M0 0l0 60l60 0L60 0L0 0M10 10L40 1L40 40L10.5 40L10 10");
        let parsed_svg = svg_d_path_to_geometry_collection(&svg_string);
        assert_eq!(parsed_svg.is_ok(), true);
        let geom = parsed_svg.ok().unwrap();
        assert_eq!(1, geom.0.len());
        let pl = Polygon::try_from(geom.0[0].clone());
        assert_eq!(true, pl.is_ok());
        assert_eq!(pl.unwrap(), poly);
    }

    #[test]
    fn can_convert_svg_path_test() {
        let poly: Polygon<f64> = polygon!(
        exterior: [
            (x: 60.0, y: 0.0),
            (x: 0.0, y: 0.0),
            (x: 0.0, y: 60.0),
            (x: 60.0, y: 60.0),
            (x: 60.0, y: 0.0),],
        interiors:[[
            (x: 10.50, y: 40.0),
            (x: 10.0, y: 10.0),
            (x: 40.0, y: 1.0),
            (x: 40.0, y: 40.0),
            (x: 10.50, y: 40.0),]
            ]
        )
        .into();
        let svg_string =
            String::from(r#"<path d="M0 0L0 60L60 60L60 0L0 0M10 10L40 1L40 40L10.5 40L10 10"/>"#);
        let parsed_svg = svg_to_geometry_collection(&svg_string, false);
        assert_eq!(parsed_svg.is_ok(), true);
        let geom = parsed_svg.ok().unwrap();
        assert_eq!(1, geom.geometry.0.len());
        let pl = Polygon::try_from(geom.geometry.0[0].clone());
        assert_eq!(true, pl.is_ok());
        assert_eq!(pl.unwrap(), poly);
    }

    #[test]
    fn can_convert_svg_h_v_path_test() {
        let poly: Polygon<f64> = polygon!(
        exterior: [
            (x: 60.0, y: 0.0),
            (x: 0.0, y: 0.0),
            (x: 0.0, y: 60.0),
            (x: 60.0, y: 60.0),
            (x: 60.0, y: 0.0),],
        interiors:[[
            (x: 10.50, y: 40.0),
            (x: 10.0, y: 10.0),
            (x: 40.0, y: 1.0),
            (x: 40.0, y: 40.0),
            (x: 10.50, y: 40.0),]
            ]
        )
        .into();
        let svg_string =
            String::from(r#"<path d="M0 0v60h60v-60h-60M10 10L40 1L40 40L10.5 40L10 10"/>"#);
        let parsed_svg = svg_to_geometry_collection(&svg_string, false);
        assert_eq!(parsed_svg.is_ok(), true);
        let geom = parsed_svg.ok().unwrap();
        assert_eq!(1, geom.geometry.0.len());
        let pl = Polygon::try_from(geom.geometry.0[0].clone());
        assert_eq!(true, pl.is_ok());
        assert_eq!(pl.unwrap(), poly);
    }

    #[test]
    fn can_convert_svg_c_s_path_test() {
        let solution = String::from(
            r#"<path d="M60 0L0 0L0.02296267872462437 1.4286271448782748L0.09102461437453917 2.8266098481062514L0.20294665616380686 4.1941959398411175L0.3574896533064902 5.531633250240061L0.5534144550166513 6.83916960946027L0.7894819105083531 8.11705284765893L1.064452868995658 9.36553079499323L1.3770881796926289 10.58485128162036L1.7261486918133275 11.7752621376975L2.110395254571817 12.937011193381846L2.5285887171821595 14.070346278830577L2.979489928858418 15.17551522420089L3.4618597388146553 16.25276585964997L3.974458996264932 17.30234601533499L4.516048550423314 18.324503521413163L5.0853892505038605 19.319486208041656L5.681241945720635 20.287541905377665L6.302367485287703 21.22891844357838L6.947526718419123 22.143863652800988L7.615480494328956 23.032625363202655L8.304989662231272 23.8954514049406L9.014815071340129 24.732589608172L9.743717570869588 25.544287803054033L10.490458010033713 26.330793819743896L11.253797238046566 27.092355488398763L12.032496104122215 27.82922063917585L12.825315457474712 28.541637102232308L13.631016147318128 29.229852707725357L14.448359022866523 29.894115285812166L15.27610493333396 30.534672666649932L16.113014727934498 31.151772680395837L16.957849255882206 31.745663157207062L17.809369366391135 32.31659192724079L18.666335908675364 32.864806820654245L19.527509731948946 33.39055566760457L20.39165168542595 33.89408629824899L21.257522618320422 34.37564654274467L22.123883379846447 34.8354842312488L22.989494819218066 35.27384719391856L23.85311778564935 35.69098326091115L24.713513128354368 36.08714026238376L25.569441696547184 36.46256602849357L26.419664339441848 36.81750838939777L27.262941906252433 37.15221517525355L28.098035246192996 37.46693421621809L28.923705208477596 37.76191334244859L29.738712642320298 38.03740038410221L30.541818396935174 38.29364317133617L31.33178332153628 38.53088953430765L32.107368265337676 38.749387303173826L32.86733407755343 38.949384308091894L33.610441607397604 39.13112837921903L34.33545170408425 39.29486734671244L35.04112521682743 39.4408490407293L35.72622299484123 39.569321291426796L36.389505887339695 39.68053192896212L37.029734743536885 39.774728783492456L37.64567041264687 39.852159685175L38.23607374388371 39.91307246416693L38.79970558646147 39.957714950625444L39.3353267895942 39.9863349747077L40 40L40.970051887601684 40.065550039331356L41.87754189800119 40.25622383306702L42.72695227439238 40.56305689481936L43.522765259969034 40.977084738200766L44.269463097924955 41.48934287682361L44.971528031453964 42.09086682430029L45.633442303749874 42.772692094243176L46.25968815800651 43.525854200264654L46.85474783741763 44.3413886559771L47.42310358517709 45.2103309749929L47.96923764447867 46.12371667092442L48.49763225851619 47.072581257384066L49.01276967048348 48.04796024798421L49.51913212357432 49.040889156337215L50.02120186098254 50.042403496055485L50.52346112590194 51.043538780751405L51.03039216152633 52.035330524037334L51.54647721104952 53.00881423952567L52.07619851766532 53.95502544082878L52.62403832456753 54.86499964155906L53.194478874949965 55.72977235532888L53.79200241200645 56.54037909575062L54.421091178930766 57.28785537643669L55.08622741891675 57.963236710999425L55.791893375158196 58.55755861305125L56.54257129084891 59.061856596204514L57.34274340918272 59.46716617407161L58.19689197335341 59.76452286026492L60 60L60 0M10 20L10 10L20 10L20 20L10 20"/>"#,
        );
        let svg_string = String::from(
            r#"<path d="M0 0C0 30 30 40 40 40S50 60 60 60L60 0ZM10 10L20 10L20 20L10 20L10 10" />"#,
        );
        let parsed_svg = svg_to_geometry_collection(&svg_string, false);
        assert_eq!(true, parsed_svg.is_ok());
        let svg = parsed_svg.ok().unwrap().to_svg();
        assert_eq!(solution, svg);
    }

    #[test]
    fn can_convert_svg_q_t_path_test() {
        let solution = String::from(
            r#"<path d="M60 0L0 0L1.0189253690266245 1.354677781295119L2.026182605832108 2.686019298147956L3.02177171041645 3.9940245505585095L4.005692682779651 5.278693538526782L4.977945522921711 6.540026262052772L5.9385302308426295 7.7780227211364785L6.887446806542407 8.992682915777905L7.824695250021044 10.184006845977049L8.750275561278539 11.351994511733908L9.664187740314894 12.496645913048486L10 12.913995710214087L10 10L20 10L20 20L15.858391915557378 20L16.555432412644645 20.813751603642952L17.364331401690727 21.748376624976984L18.161562258515673 22.65966538186874L18.947124983119473 23.547617874318213L19.72101957550213 24.412234102325407L20.48324603566365 25.253514065890315L21.23380436360403 26.071457765012937L21.97269455932327 26.866065199693285L22.699916622821366 27.637336369931347L23.41547055409832 28.385271275727128L24.11935635315413 29.109869917080623L24.811574019988804 29.811132293991832L25.492123554602337 30.48905840646077L26.161004956994724 31.143648254487424L26.81821822716598 31.77490183807179L27.463763365116083 32.38281915721387L28.09764037084505 32.96740021191368L28.719849244352872 33.528645002171196L29.330389985639563 34.06655352798644L29.929262594705097 34.58112578935939L30.516467071549506 35.07236178629007L31.092003416172766 35.54026151877846L31.655871628574886 35.98482498682457L32.20807170875587 36.4060521904284L32.7486036567157 36.80394312958994L33.2774674724544 37.1784978043092L33.79466315597195 37.52971621458619L34.30019070726837 37.85759836042089L34.79405012634364 38.1621442418133L35.27624141319777 38.44335385876343L35.746764567830766 38.70122721127128L36.20561959024262 38.93576429933685L36.65280648043332 39.14696512296014L37.08832523840288 39.33482968214114L37.51217586415131 39.49935797687987L37.9243583576786 39.64055000717631L38.32487271898474 39.75840577303047L38.71371894806975 39.85292527444234L39.090897044933605 39.92410851141193L39.456407009576324 39.97195548393924L40 40L40.64806292823906 40.02099927794789L41.29612585647813 40.083997111791554L41.944188784717184 40.188993501531L42.592251712956255 40.33598844716624L43.24031464119532 40.52498194869724L43.88837756943438 40.75597400612402L44.536440497673446 41.02896461944658L45.18450342591251 41.343953788664926L45.83256635415157 41.70094151377904L46.48062928239064 42.09992779478895L47.12869221062971 42.540912631694624L47.776755138868765 43.02389602449608L48.42481806710782 43.54887797319331L49.072880995346885 44.11585847778633L49.72094392358594 44.724837538275125L50.36900685182502 45.375815154659705L51.01706978006408 46.06879132694006L51.66513270830314 46.803766055116185L52.313195636542204 47.5807393391881L52.961258564781275 48.39971117915579L53.60932149302033 49.260681575019255L54.257384421259395 50.163650526778504L54.905447349498445 51.10861803443353L55.553510277737516 52.09558409798433L56.20157320597659 53.124548717430926L56.84963613421565 54.19551189277328L57.49769906245471 55.30847362401143L58.14576199069378 56.463433911145344L58.793824918932835 57.66039275417504L60 60L60 0"/>
<path d="M10.566431787130107 13.617961049920781L10 12.913995710214087L10 20L15.858391915557378 20L15.73486529137742 19.855790317866628L14.902630037889054 18.87449276764803L14.058726652179544 17.86985895298714L13.203155134248899 16.841888873883974L12.335915484097109 15.79058253033853L11.457007701724176 14.715939922350795L10.566431787130107 13.617961049920781"/>"#,
        );
        let svg_string = String::from(
            r#"<path d="M0 0Q30 40 40 40T60 60L60 0ZM10 10L20 10L20 20L10 20L10 10" />"#,
        );
        let parsed_svg = svg_to_geometry_collection(&svg_string, false);
        assert_eq!(true, parsed_svg.is_ok());
        let svg = parsed_svg.ok().unwrap().to_svg();
        assert_eq!(solution, svg);
    }

    #[test]
    fn can_convert_svg_polygon_test() {
        let poly: Polygon<f64> = polygon!(
        exterior: [
            (x: 0.0, y: 0.0),
            (x: 0.0, y: 60.0),
            (x: 60.0, y: 60.0),
            (x: 60.0, y: 0.0),
            (x: 0.0, y: 0.0),],
        interiors:[]
        )
        .into();
        let svg_string = String::from(r#"<polygon points="0, 0 60, 0 60, 60 0, 60 0, 0"/>"#);
        let parsed_svg = svg_to_geometry_collection(&svg_string, false);
        assert_eq!(parsed_svg.is_ok(), true);
        let geom = parsed_svg.ok().unwrap();
        assert_eq!(1, geom.geometry.0.len());
        let pl = Polygon::try_from(geom.geometry.0[0].clone());
        assert_eq!(true, pl.is_ok());
        assert_eq!(pl.unwrap(), poly);
    }

    #[test]
    fn can_convert_svg_polyline_test() {
        let line: LineString<f64> = line_string![
            (x: 0.0, y: 0.0),
            (x: 0.0, y: 60.0),
            (x: 60.0, y: 60.0),
            (x: 60.0, y: 0.0),]
        .into();
        let svg_string = String::from(r#"<polyline points="0, 0 0, 60 60, 60 60, 0"/>"#);
        let parsed_svg = svg_to_geometry_collection(&svg_string, false);
        assert_eq!(parsed_svg.is_ok(), true);
        let geom = parsed_svg.ok().unwrap();
        assert_eq!(1, geom.geometry.0.len());
        let pl = LineString::try_from(geom.geometry.0[0].clone());
        assert_eq!(true, pl.is_ok());
        assert_eq!(line, pl.unwrap());
    }

    #[test]
    fn can_convert_svg_rect_test() {
        let expected = Rect::new(coord! { x: 0.0, y: 0.0 }, coord! { x: 60.0, y: 60.0 });
        let svg_string = String::from(r#"<rect x="0" y="0" width="60" height="60"/>"#);
        let parsed_svg = svg_to_geometry_collection(&svg_string, false);
        assert_eq!(parsed_svg.is_ok(), true);
        let geom = parsed_svg.ok().unwrap();
        assert_eq!(1, geom.geometry.0.len());
        let pl = Rect::try_from(geom.geometry.0[0].clone());
        assert_eq!(true, pl.is_ok());
        assert_eq!(expected, pl.unwrap());
    }

    #[test]
    fn can_convert_svg_path_to_single_geom() {
        let poly: Polygon<f64> = polygon!(
            exterior: [
                (x: 60.0, y: 0.0),
                (x: 0.0, y: 0.0),
                (x: 0.0, y: 60.0),
                (x: 60.0, y: 60.0),
                (x: 60.0, y: 0.0),],
            interiors:[[
                (x: 10.50, y: 40.0),
                (x: 10.0, y: 10.0),
                (x: 40.0, y: 1.0),
                (x: 40.0, y: 40.0),
                (x: 10.50, y: 40.0),]
                ]
            );
        let svg_string =
            String::from(r#"<path d="M0 0L0 60L60 60L60 0L0 0M10 10L40 1L40 40L10.5 40L10 10"/>"#);

        let parsed_svg = svg_to_geometry(&svg_string, false);
        assert!(parsed_svg.is_ok());
        let parsed_poly = Polygon::try_from(parsed_svg.ok().unwrap().geometry);
        assert!(parsed_poly.is_ok());
        assert_eq!(poly, parsed_poly.unwrap());
    }

    #[test]
    fn can_convert_svg_polygon_to_single_geom() {
        let poly: Polygon<f64> = polygon!(
            exterior: [
                (x: 0.0, y: 0.0),
                (x: 0.0, y: 60.0),
                (x: 60.0, y: 60.0),
                (x: 60.0, y: 0.0),
                (x: 0.0, y: 0.0),],
            interiors:[]
            );
        let svg_string = String::from(r#"<polygon points="0, 0 60, 0 60, 60 0, 60 0, 0"/>"#);

        let parsed_svg = svg_to_geometry(&svg_string, false);
        assert!(parsed_svg.is_ok());
        let parsed_poly = Polygon::try_from(parsed_svg.ok().unwrap().geometry);
        assert!(parsed_poly.is_ok());
        assert_eq!(poly, parsed_poly.unwrap());
    }

    #[test]
    fn svg_file_parse_works() {
        let file = include_str!("../../test/test.svg");
        let parsed = svg_file_to_geometry_collections(file, false).unwrap();
        assert_eq!(parsed.elements.len(), 3);
        assert_eq!(parsed.elements[0].style.len(), 0);
    }

    #[test]
    fn svg_style_parse_works() {
        let file = include_str!("../../test/test.svg");
        let parsed = svg_file_to_geometry_collections(file, true).unwrap();

        assert_eq!(parsed.elements[0].style.len(), 1);
        assert!(parsed.elements[0].style.get("stroke-width").is_some());
    }
}
